package com.sumerge.junit;

import com.sumerge.rest_api.beans.StudentFilterBean;
import com.sumerge.rest_api.model.Student;
import com.sumerge.rest_api.resources.student.StudentResource;
import com.sumerge.rest_api.service.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentTest {
    @InjectMocks
    private StudentResource studentResource;

    @Mock
    private StudentService studentService;

    private Map<Long , Student> studentMap;
    private List<Student> studentList;
    private Student addedStudent;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        studentMap = new HashMap<>();
        studentMap.put(1L , new Student(1 , "Sam" , 2014 , "CS"));
        studentMap.put(2L , new Student(2 , "Mark" , 2016 , "ME"));
        studentMap.put(3L , new Student(3 , "David" , 2020 , "EE"));
        studentList = new ArrayList<>(studentMap.values());
        addedStudent = new Student(6L , "Mark" , 2019 , "CS");
    }

    @Nested
    @DisplayName("Get testing")
    class get {
        @Test
        @DisplayName("Testing get all students")
        public void testGet() {
            when(studentService.getAllStudents(anyLong())).thenReturn(studentList);
            Response response = studentResource.getAllStudents(anyLong() , new StudentFilterBean());
            assertTrue(response.getEntity().equals(studentList) ,
                    "It should return all the list");
        }

        @Nested
        @DisplayName("Testing get by Id")
        class GetById {

            private void testSpecificID(Long l) {
                when(studentService.getStudent(0L , l)).thenReturn(studentMap.get(l));
                Response response = studentResource.getStudentById(0L , l);
                assertTrue(response.getEntity().equals(studentMap.get(l)) ,
                        "This should get the student having this Id");
            }

            @Test
            @DisplayName("Testing get by Id with valid Id")
            public void testGetByValidId() {
                assertAll(
                        () -> testSpecificID(1L) ,
                        () -> testSpecificID(2L) ,
                        () -> testSpecificID(3L) );
            }

            @Test
            @DisplayName("Testing get by Id with invalid Id")
            public void testGetByInvalidId() {
//                when(studentService.getStudent(0L , -1L)).thenReturn(null);
                assertThrows(WebApplicationException.class , () -> studentResource.getStudentById(0L , -1L) ,
                        "Page not found exception should be thrown");
            }
        }
    }

    @Test
    @DisplayName("Testing post method")
    public void testPost() throws URISyntaxException {
        when(studentService.addStudent(0L , addedStudent))
                .thenReturn(addedStudent);
        UriInfo uriInfo = mock(UriInfo.class);
        when(uriInfo.getAbsolutePath())
                .thenReturn(new URI("http://localhost:8080/app/classes/" + 0 + "/students/"));
        Response response = studentResource.addStudent(uriInfo , 0L , addedStudent);
        assertTrue(response.getEntity().equals(addedStudent));
    }
}