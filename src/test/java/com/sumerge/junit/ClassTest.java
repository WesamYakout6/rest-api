package com.sumerge.junit;

import com.sumerge.rest_api.beans.ClassFilterBean;
import com.sumerge.rest_api.model.Class;
import com.sumerge.rest_api.resources.Class.ClassResource;
import com.sumerge.rest_api.service.ClassService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DisplayName("Testing class resource")
public class ClassTest {

    @InjectMocks
    private ClassResource classResource;

    @Mock
    private ClassService classService;

    private Map<Long , Class> classMap;
    private List<Class> classList;
    private Class addedClass;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        classMap = new HashMap<>();
        classMap.put(1L , new Class(1 , "CS" , null));
        classMap.put(2L , new Class(2 , "ME" , null));
        classMap.put(3L , new Class(3 , "EE" , null));
        classMap.put(4L , new Class(4 , "CS" , null));
        classList = new ArrayList<>(classMap.values());
        addedClass = new Class(6L , "EE" , null);
    }

    @Nested
    @DisplayName("Get testing")
    class get {
        @Test
        @DisplayName("Testing get all classes")
        public void testGet() {
            when(classService.getAllClasses()).thenReturn(classList);
            Response response = classResource.getAllClasses(new ClassFilterBean());
            assertTrue(response.getEntity().equals(classList) ,
                    "It should return all the list");
        }

        @Test
        @DisplayName("Testing get all classes filtered by valid Id")
        public void testGetFilterByValidId() {
            List<Class> filteredList = new ArrayList<>();
            filteredList.add(classMap.get(1L));
            when(classService.getAllClasses()).thenReturn(filteredList);
            ClassFilterBean classFilterBean = new ClassFilterBean();
            classFilterBean.setId(1L);
            Response response = classResource.getAllClasses(classFilterBean);
            assertTrue(response.getEntity().equals(filteredList) ,
                    "It should return all the list");
        }

        @Test
        @DisplayName("Testing get all classes filtered by invalid Id")
        public void testGetByInvalidId() {
            ClassFilterBean classFilterBean = new ClassFilterBean();
            classFilterBean.setId(-1L);
            assertThrows(WebApplicationException.class , () -> classResource.getAllClasses(classFilterBean) ,
                    "Page not found exception should be thrown");
            classFilterBean.setId(100L);
            assertThrows(WebApplicationException.class , () -> classResource.getAllClasses(classFilterBean) ,
                    "Page not found exception should be thrown");
        }

        @Test
        @DisplayName("Testing get all classes filtered by Existing Names")
        public void testGetFilterByExistingName() {
            List<Class> filteredList = new ArrayList<>();
            filteredList.add(classMap.get(1L));
            filteredList.add(classMap.get(4L));
            when(classService.getAllClasses()).thenReturn(filteredList);
            ClassFilterBean classFilterBean = new ClassFilterBean();
            classFilterBean.setName("CS");
            Response response = classResource.getAllClasses(classFilterBean);
            assertTrue(response.getEntity().equals(filteredList) ,
                    "It should return all the list");
        }

        @Test
        @DisplayName("Testing get all classes filtered by not Existing Names")
        public void testGetFilterByNotExistingName() {
            ClassFilterBean classFilterBean = new ClassFilterBean();
            classFilterBean.setName("CS");
            assertThrows(WebApplicationException.class , () -> classResource.getAllClasses(classFilterBean) ,
                    "Page not found exception should be thrown");
        }

        @Test
        @DisplayName("Testing get all classes paging")
        public void testGetPagingUsingValidStartAndSize() {
            List<Class> filteredList = new ArrayList<>();
            filteredList.add(classMap.get(2L));
            filteredList.add(classMap.get(3L));
            when(classService.getAllClasses()).thenReturn(classList);
            ClassFilterBean classFilterBean = new ClassFilterBean();
            classFilterBean.setStart(1);
            classFilterBean.setSize(2);
            Response response = classResource.getAllClasses(classFilterBean);
            assertTrue(response.getEntity().equals(filteredList) ,
                    "It should return all the list");
        }

        @Test
        @DisplayName("Testing get all classes paging with in valid size and start")
        public void testGetPagingUsingINValidStartAndSize() {
            assertAll(
                    () -> getClassFilterBean(-1 , 2) ,
                    () -> getClassFilterBean(100 , 3) ,
                    () -> getClassFilterBean(1 , 200) ,
                    () -> getClassFilterBean(1 , -20)
            );
        }

        private void getClassFilterBean(int start , int size) {
            ClassFilterBean classFilterBean = new ClassFilterBean();
            classFilterBean.setStart(start);
            classFilterBean.setSize(size);
            assertThrows(WebApplicationException.class , () -> classResource.getAllClasses(classFilterBean) ,
                    "Page not found exception should be thrown");
        }


        @Nested
        @DisplayName("Testing get by Id")
        class GetById {

            private void testSpecificID(Long l) {
                when(classService.getClass(l)).thenReturn(classMap.get(l));
                Response response = classResource.getClassById(l);
                assertTrue(response.getEntity().equals(classMap.get(l)) ,
                        "This should get the class of this Id");
            }

            @Test
            @DisplayName("Testing get by Id with valid Id")
            public void testGetByValidId() {
//        fail("Not implemented yet");
                assertAll(
                        () -> testSpecificID(1L) ,
                        () -> testSpecificID(2L) ,
                        () -> testSpecificID(3L) );
            }

            @Test
            @DisplayName("Testing get by Id with invalid Id")
            public void testGetByInvalidId() {
                assertThrows(WebApplicationException.class , () -> classResource.getClassById(-1L) ,
                        "Page not found exception should be thrown");
                assertThrows(WebApplicationException.class , () -> classResource.getClassById(100L) ,
                        "Page not found exception should be thrown");
            }
        }
    }

    @Test
    @DisplayName("Testing post method")
    public void testPost() throws URISyntaxException {
        when(classService.addClass(addedClass))
                .thenReturn(addedClass);
        UriInfo uriInfo=Mockito.mock(UriInfo.class);
        Mockito.when(uriInfo.getAbsolutePath())
                .thenReturn(URI.create("http://localhost:8080/app/classes"));
        Response response = classResource.addClass(uriInfo , addedClass);
        assertTrue(response.getEntity().equals(addedClass));
    }
}
