package com.sumerge.rest_api.model;


public class ErrorMessage {
    private int code;
    private String message;
    private String instruction;

    public ErrorMessage() {
    }

    public ErrorMessage(int code, String message, String instruction) {
        this.code = code;
        this.message = message;
        this.instruction = instruction;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
