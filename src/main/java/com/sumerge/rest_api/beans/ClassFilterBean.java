package com.sumerge.rest_api.beans;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public class ClassFilterBean {
    private @QueryParam("name") String name;
    private @QueryParam("id") Long id;
    private @QueryParam("start") int start;
    private @QueryParam("size") int size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
