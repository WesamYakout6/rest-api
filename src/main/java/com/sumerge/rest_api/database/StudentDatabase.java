package com.sumerge.rest_api.database;

import com.sumerge.rest_api.model.Class;
import com.sumerge.rest_api.model.Student;

import java.util.HashMap;
import java.util.Map;

public class StudentDatabase {

    private static Map<Long , Student> studentMap = new HashMap<>();

    public static Map<Long , Student> getStudentMap() {
        studentMap.put(1L , new Student(1 , "Sam" , 2014 , "CS"));
        studentMap.put(2L , new Student(2 , "Mark" , 2016 , "ME"));
        studentMap.put(3L , new Student(3 , "David" , 2020 , "EE"));
        return studentMap;
    }

}
