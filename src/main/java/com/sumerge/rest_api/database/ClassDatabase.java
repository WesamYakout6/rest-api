package com.sumerge.rest_api.database;

import com.sumerge.rest_api.model.Class;
import com.sumerge.rest_api.model.Student;

import java.util.HashMap;
import java.util.Map;

public class ClassDatabase {
    private static Map<Long , Class> classMap = new HashMap<>();
    private static Map<Long , Student> studentMap = StudentDatabase.getStudentMap();

    public static Map<Long , Class> getClassMap() {
        classMap.put(1L , new Class(1 , "CS" , studentMap));
        classMap.put(2L , new Class(2 , "ME" , studentMap));
        classMap.put(3L , new Class(3 , "EE" , studentMap));
        return classMap;
    }
}
