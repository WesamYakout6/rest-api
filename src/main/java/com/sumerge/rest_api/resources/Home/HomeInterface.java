package com.sumerge.rest_api.resources.Home;

import com.sumerge.rest_api.resources.application.ApplicationResource;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/")
public interface HomeInterface {

    @Path("/{app}")
    ApplicationResource getApplication(@PathParam("app") String app);
}
