package com.sumerge.rest_api.resources.Home;

import com.sumerge.rest_api.model.ErrorMessage;
import com.sumerge.rest_api.resources.application.ApplicationResource;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class HomeResource implements HomeInterface{
    @Override
    public ApplicationResource getApplication(String app) {
        ErrorMessage errorMessage = new ErrorMessage(500 ,
                "Internal Service Error!!" , "Go to https://localhost:8080/app");
        Response response = Response.serverError()
                                    .type(MediaType.APPLICATION_JSON)
                                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                                    .entity(errorMessage).build();
        if(app.equals("app"))
            return new ApplicationResource();
        throw new WebApplicationException(response);
    }
}
