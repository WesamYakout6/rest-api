package com.sumerge.rest_api.resources.student;

import com.sumerge.rest_api.model.ErrorMessage;
import com.sumerge.rest_api.model.Student;
import com.sumerge.rest_api.beans.StudentFilterBean;
import com.sumerge.rest_api.service.StudentService;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


public class StudentResource implements StudentInterface {

    StudentService studentService = new StudentService();

    @Override
    public Response getAllStudents(Long classId , StudentFilterBean studentFilterBean) {
        List<Student> result = studentService.getAllStudents(classId);
        if(studentFilterBean.getId() != null)
            result = result.stream().filter(s -> s.getId() == studentFilterBean.getId())
                           .collect(Collectors.toList());
        if(studentFilterBean.getName() != null)
            result = result.stream().filter(s -> s.getName().equals(studentFilterBean.getName()))
                           .collect(Collectors.toList());
        if(studentFilterBean.getYear() > 0)
            result = result.stream().filter(s -> s.getEnrollmentYear() == studentFilterBean.getYear())
                           .collect(Collectors.toList());
        if(studentFilterBean.getMajor() != null)
            result = result.stream().filter(s -> s.getMajor().equals(studentFilterBean.getMajor()))
                    .collect(Collectors.toList());
        if(studentFilterBean.getStart() >= 0 && studentFilterBean.getSize() > 0) {
            if(studentFilterBean.getStart() + studentFilterBean.getSize() > result.size()) {
                ErrorMessage errorMessage = new ErrorMessage(404 ,
                        "Not Found!!" ,
                        "Paging exceeds the list");
                throw new WebApplicationException(javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND)
                        .entity(errorMessage).build());
            }
            else
                return Response.ok(result.subList(studentFilterBean.getStart() , studentFilterBean.getStart()
                        + studentFilterBean.getSize())).build();
        }
        return Response.ok(result).build();
    }

    @Override
    public Response getStudentById(Long classId , Long studentId) {
        return Response.ok(studentService.getStudent(classId , studentId)).build();
    }

    @Override
    public Response addStudent(UriInfo uriInfo , Long classId , Student student) {
        Student stud =  studentService.addStudent(classId , student);
        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(stud.getId())).build();
        return Response.created(uri).entity(stud).build();
    }

    @Override
    public Response updateStudent(UriInfo uriInfo , Long classId , Long studentId , Student student) {
        student.setId(studentId);
        Student stud =  studentService.update(classId , student);
        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(stud.getId())).build();
        return Response.created(uri).entity(stud).build();
    }

    @Override
    public Response deleteStudent(Long classId , Long studentId) {
        return Response.ok().status(Response.Status.NO_CONTENT)
                       .entity(studentService.deleteStudent(classId , studentId)).build();
    }
}
