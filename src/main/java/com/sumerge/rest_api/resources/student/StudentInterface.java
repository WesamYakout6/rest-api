package com.sumerge.rest_api.resources.student;

import com.sumerge.rest_api.model.Student;
import com.sumerge.rest_api.beans.StudentFilterBean;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/students")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface StudentInterface {

    @GET
    Response getAllStudents(@PathParam("classId") Long classId , @BeanParam StudentFilterBean studentFilterBean);

    @GET
    @Path("/{studentId}")
    Response getStudentById(@PathParam("classId") Long classId , @PathParam("studentId") Long studentId);

    @POST
    Response addStudent(@Context UriInfo uriInfo , @PathParam("classId") Long classId , Student student);

    @PUT
    @Path("/{studentId}")
    Response updateStudent(@Context UriInfo uriInfo , @PathParam("classId") Long classId ,
                           @PathParam("studentId") Long studentId , Student student);

    @DELETE
    @Path("/{studentId}")
    Response deleteStudent(@PathParam("classId") Long classId , @PathParam("studentId") Long studentId);
}
