package com.sumerge.rest_api.resources.application;

import com.sumerge.rest_api.model.ErrorMessage;
import com.sumerge.rest_api.resources.Class.ClassResource;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ApplicationResource implements ApplicationInterface {
    @Override
    public ClassResource classResource(String classes) {
        ErrorMessage errorMessage = new ErrorMessage(502 ,
                "Bad Gatway!! You Should direct to classes" ,
                "Go to https://localhost:8080/app/classes");
        Response response = Response.serverError()
                .type(MediaType.APPLICATION_JSON)
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessage).build();
        if(classes.equals("classes"))
            return new ClassResource();
        throw new WebApplicationException(response);
    }
}
