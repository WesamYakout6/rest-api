package com.sumerge.rest_api.resources.application;

import com.sumerge.rest_api.resources.Class.ClassResource;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/app")
public interface ApplicationInterface {

    @Path("/{class}")
    ClassResource classResource(@PathParam("class") String classes);
}
