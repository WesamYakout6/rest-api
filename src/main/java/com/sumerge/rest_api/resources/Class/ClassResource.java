package com.sumerge.rest_api.resources.Class;

import com.sumerge.rest_api.model.Class;
import com.sumerge.rest_api.model.ErrorMessage;
import com.sumerge.rest_api.beans.ClassFilterBean;
import com.sumerge.rest_api.resources.student.StudentResource;
import com.sumerge.rest_api.service.ClassService;

import javax.ws.rs.BeanParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ClassResource implements ClassInterface {

    ClassService classService = new ClassService();


    @Override
    public Response getAllClasses(@BeanParam ClassFilterBean classFilterBean) {
        List<Class> result = classService.getAllClasses();
        if(classFilterBean.getName() != null)
            result = result.stream().filter((c) -> c.getName().equals(classFilterBean.getName()))
                    .collect(Collectors.toList());
        if(classFilterBean.getId() != null)
            result = result.stream().filter(c -> c.getId() == classFilterBean.getId())
                    .collect(Collectors.toList());
        if(classFilterBean.getStart() >= 0 && classFilterBean.getSize() > 0) {
//            System.out.println("start: " + classFilterBean.getStart() + " size: " + classFilterBean.getSize()
//            + " array size: " + result.size());
            if(classFilterBean.getStart() + classFilterBean.getSize() > result.size())
                result = new ArrayList<>();
            else
                result = result.subList(classFilterBean.getStart() , classFilterBean.getStart()
                        + classFilterBean.getSize());
        }
        if(result.isEmpty())
        {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "The list is empty");
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                                                      .entity(errorMessage).build());
        }
        return Response.ok(result).build();
    }

    @Override
    public Response getClassById(Long id) {
        if(classService.getClass(id) == null) {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "The item You chose doesn`t exist");
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                    .entity(errorMessage).build());
        }
        return Response.ok(classService.getClass(id)).build();
    }

    @Override
    public Response addClass(UriInfo uriInfo , Class c) throws URISyntaxException {
        Class classs =  classService.addClass(c);
//        URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(classs.getId())).build();
        URI uri = new URI(uriInfo.getAbsolutePath() + String.valueOf(classs.getId()));
        return Response.created(uri).entity(classs).build();
    }

    @Override
    public Response updateClass(UriInfo uriInfo , Long id , Class c) {
        c.setId(id);
        if(id > 0 && classService.getClass(c.getId()) != null)
        {
            Class classs = classService.update(c);
            URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(classs.getId())).build();
            return Response.created(uri).entity(classs).build();
        }
        ErrorMessage errorMessage = new ErrorMessage(404 ,
                "Not Found!!" ,
                "The item You chose doesn`t exist");
        throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                .entity(errorMessage).build());
    }

    @Override
    public Response deleteClass(Long id) {
        if(id > 0 && classService.getClass(id) != null)
            return Response.ok().status(Response.Status.NO_CONTENT).entity(classService.delete(id)).build();
        ErrorMessage errorMessage = new ErrorMessage(404 ,
                "Not Found!!" ,
                "The item You chose doesn`t exist");
        throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                .entity(errorMessage).build());
    }

    @Override
    public StudentResource getStudentResource(String students) {
        if(students.equals("students"))
            return new StudentResource();
        ErrorMessage errorMessage = new ErrorMessage(500 ,
                "Internal Server Error!!" ,
                "You should add classId followed by students");
        Response response = Response.serverError()
                .type(MediaType.APPLICATION_JSON)
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errorMessage).build();
        throw new WebApplicationException(response);
    }

}
