package com.sumerge.rest_api.resources.Class;

import com.sumerge.rest_api.model.Class;
import com.sumerge.rest_api.beans.ClassFilterBean;
import com.sumerge.rest_api.resources.student.StudentResource;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;

@Path("/classes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ClassInterface {
    @GET
    Response getAllClasses(@BeanParam ClassFilterBean classFilterBean);

    @GET
    @Path("/{classId}")
    Response getClassById(@PathParam("classId") Long id);

    @POST
    Response addClass(@Context UriInfo uriInfo , Class c) throws URISyntaxException;

    @PUT
    @Path("/{classId}")
    Response updateClass(@Context UriInfo uriInfo , @PathParam("classId") Long id , Class c);

    @DELETE
    @Path("/{classId}")
    Response deleteClass(@PathParam("classId") Long id);

    @Path("/{classId}/{students}")
    StudentResource getStudentResource(@PathParam("students") String students);
}
