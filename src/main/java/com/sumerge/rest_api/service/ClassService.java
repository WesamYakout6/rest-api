package com.sumerge.rest_api.service;

import com.sumerge.rest_api.database.ClassDatabase;
import com.sumerge.rest_api.model.Class;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassService {

    private Map<Long, Class> classes = ClassDatabase.getClassMap();

    public List<Class> getAllClasses() {
        return new ArrayList<>(classes.values());
    }

    public Class getClass(long id) {
        return classes.get(id);
    }

    public Class addClass(Class c) {
        c.setId(classes.size() + 1L);
        classes.put(c.getId(), c);
        return c;
    }

    public Class update(Class c) {
        if (c.getId() <= 0)
            return null;
        classes.put(c.getId(), c);
        return c;
    }

    public Class delete(Long id) {
        return classes.remove(id);
    }

}
