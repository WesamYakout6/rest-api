package com.sumerge.rest_api.service;

import com.sumerge.rest_api.database.ClassDatabase;
import com.sumerge.rest_api.model.Class;
import com.sumerge.rest_api.model.ErrorMessage;
import com.sumerge.rest_api.model.Student;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentService {

    private static Map<Long , Class> classes = ClassDatabase.getClassMap();

    public List<Student> getAllStudents(Long classId) {
        Map<Long, Student> students = checkClass(classId);
        if(students.isEmpty())
        {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "This class has no students");
            throw new WebApplicationException(javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND)
                    .entity(errorMessage).build());
        }
        return new ArrayList<>(students.values());
    }

    private Map<Long , Student> checkClass(Long classId) {
        if(classes.get(classId) == null) {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "There is no class with this Id");
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                    .entity(errorMessage).build());
        }
        return classes.get(classId).getStudents();
    }

    public Student getStudent(Long classId , Long studentId) {
        Map<Long , Student> students = checkClass(classId);
        if(students.get(studentId) == null) {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "The StudentId You chose doesn`t exist");
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                    .entity(errorMessage).build());
        }
        return students.get(studentId);
    }

    public Student addStudent(Long classId , Student student) {
        Map<Long , Student> students = classes.get(classId).getStudents();
        student.setId(students.size() + 1L);
        students.put(student.getId() , student);
        return student;
    }

    public Student update(Long classId , Student student) {
        Map<Long , Student> students = checkClass(classId);
        if(student.getId() < 0 || students.get(student.getId()) == null) {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "The StudentId You chose doesn`t exist");
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                    .entity(errorMessage).build());
        }
        students.put(student.getId() , student);
        return student;
    }

    public Student deleteStudent(Long classId , Long studentId) {
        Map<Long , Student> students = checkClass(classId);
        if(students.get(studentId) == null) {
            ErrorMessage errorMessage = new ErrorMessage(404 ,
                    "Not Found!!" ,
                    "The StudentId You chose doesn`t exist");
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
                    .entity(errorMessage).build());
        }
        return students.remove(studentId);
    }
}
